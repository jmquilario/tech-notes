import { useEffect } from 'react'

const useTitle = (title) => {
  useEffect(() => {
    const prevTitle = document.title
    document.title = title

    // clean up function
    // whenever the component unmounts, it sets title to previous
    return () => (document.title = prevTitle)
  }, [title])
}

export default useTitle
